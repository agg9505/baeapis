﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using System.Linq;
using API.Models;
using System.Data.Entity;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

[assembly: OwinStartupAttribute(typeof(API.Startup))]
namespace API.hubs
{
    public class NotificationHub : Hub
    {
        private BAESystemsGuaymasDB db = new BAESystemsGuaymasDB();
        public class PreNotification
        {
            public string destiny { get; set; }
            public int type { get; set; }
            public string subject { get; set; }
            public string message { get; set; }
            public int creatorID { get; set; }
            public string appName { get; set; }
            public string icon { get; set; }
            public string link { get; set; }
        }
        public void SearchStartNotification(PreNotification pren)
        {
            try
            {
                string[] re = pren.destiny.Split(',');
                List<string> receivers = new List<string>(pren.destiny.Split(','));
                try
                {
                    foreach (var r in re)
                    {
                        // set up domain context
                        PrincipalContext ctx = new PrincipalContext(ContextType.Domain);

                        // find the group in question
                        GroupPrincipal group = GroupPrincipal.FindByIdentity(ctx, "r");

                        // if found....
                        if (group != null)
                        {
                            // iterate over members
                            foreach (Principal p in group.GetMembers())
                            {
                                Console.WriteLine("{0}: {1}", p.StructuralObjectClass, p.DisplayName);

                                // do whatever you need to do to those members
                                UserPrincipal theUser = p as UserPrincipal;

                                if (theUser != null)
                                {
                                    DirectoryEntry directoryEntry = theUser.GetUnderlyingObject() as DirectoryEntry;
                                    if (directoryEntry.Properties.Contains("EmployeeID"))
                                    {
                                        receivers.Add(directoryEntry.Properties["EmployeeID"].Value.ToString());
                                    }
                                }
                            }
                        }

                    }
                }
                catch (Exception)
                {

                }
                List<Employees> employeeReceiversList = db.Employees.Where(a => receivers.Contains(a.EmployeeID.ToString()) ||
                                                                  receivers.Contains(a.JobTitles.JobTitleName.ToString()) ||
                                                                  receivers.Contains(a.EmployeeNumber.ToString())).ToList();
                Notifications n = null;
                DateTime now = DateTime.Now;
                foreach (var item in employeeReceiversList)
                {
                    n = new Notifications()
                    {
                        AppName = pren.appName,
                        CreationDate = now,
                        EmployeeCreatorID = pren.creatorID,
                        Icon = pren.icon,
                        IntendedEmployeeID = item.EmployeeID,
                        IntendedGroupID = 0,
                        Link = pren.link,
                        Message = pren.message,
                        Seen = false,
                        SeenDate = null,
                        Subject = pren.subject,
                        Type = pren.type
                    };
                    db.Notifications.Add(n);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Notifications n = null;
                DateTime now = DateTime.Now;

                n = new Notifications()
                {
                    AppName = pren.appName,
                    CreationDate = now,
                    EmployeeCreatorID = pren.creatorID,
                    Icon = pren.icon,
                    IntendedEmployeeID = 0,
                    IntendedGroupID = 0,
                    Link = pren.link,
                    Message = pren.message + "Exception: " + ex.ToString(),
                    Seen = false,
                    SeenDate = null,
                    Subject = pren.subject,
                    Type = pren.type
                };
                db.Notifications.Add(n);
                db.SaveChanges();

            }
            Clients.All.searchNotifications();
        }
        public void SearchStartSimple()
        {
            Clients.All.searchNotifications();
        }
        public void GetNotifications(string data, string connID)
        {
            int d = Convert.ToInt32(data);
            List<Notifications> not = db.Notifications.Where(a => a.IntendedEmployeeID == d && a.Seen == false).OrderByDescending(a=>a.CreationDate).ToList();
            if (not.Count<10)
            {
                List<Notifications> not2 = db.Notifications.Where(a => a.IntendedEmployeeID == d && a.Seen).OrderByDescending(a => a.CreationDate).Take(10-not.Count).ToList();
                foreach (var item in not2)
                {
                    not.Add(item);
                }
            }
            if (not.Count > 0)
            {
                Clients.Client(connID).showNotification(not);
            }

        }
        public void SeenNotification(string not)
        {
            try
            {
                List<string> notIdList = new List<string>(not.Split(','));
                List<Notifications> notList = db.Notifications.Where(a =>notIdList.Contains(a.NotificationID.ToString()) &&a.Seen == false).ToList();
               
                DateTime now = DateTime.Now;
                foreach (var item in notList)
                {
                    item.Seen = true;
                    item.SeenDate = now;
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}