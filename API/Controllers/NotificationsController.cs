﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using System.DirectoryServices;
using API.Models;
using System.DirectoryServices.AccountManagement;
using API.hubs;

namespace API.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NotificationsController : ApiController
    {
       
        private BAESystemsGuaymasDB db = new BAESystemsGuaymasDB();
       
        // GET: api/Notifications
        public IQueryable<Notifications> GetNotifications()
        {
            return db.Notifications;
        }

        // GET: api/Notifications/5
        [ResponseType(typeof(Notifications))]
        public IHttpActionResult GetNotifications(int id)
        {
            List<Notifications> notifications = db.Notifications.Where(a => a.IntendedEmployeeID == id).ToList();
            if (notifications == null)
            {
                return NotFound();
            }
            return Ok(notifications);
        }

        // PUT: api/Notifications/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNotifications(int id, Notifications notifications)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != notifications.NotificationID)
            {
                return BadRequest();
            }

            db.Entry(notifications).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NotificationsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        public class PreNotification
        {
            public string destiny { get; set; }
            public int type { get; set; }
            public string subject { get; set; }
            public string message { get; set; }
            public int creatorID { get; set; }
            public string appName { get; set; }
            public string icon { get; set; }
            public string link { get; set; }
        }
        // POST: api/Notifications
        [ResponseType(typeof(PreNotification))]
        public IHttpActionResult PostNotifications(PreNotification pren)
        {
            try
            {
                string[] re = pren.destiny.Split(',');
                List<string> receivers = new List<string>(pren.destiny.Split(','));
                try
                {
                    foreach (var r in re)
                    {
                        // set up domain context
                        PrincipalContext ctx = new PrincipalContext(ContextType.Domain);

                        // find the group in question
                        GroupPrincipal group = GroupPrincipal.FindByIdentity(ctx, r);

                        // if found....
                        if (group != null)
                        {
                            // iterate over members
                            foreach (Principal p in group.GetMembers())
                            {
                                Console.WriteLine("{0}: {1}", p.StructuralObjectClass, p.DisplayName);

                                // do whatever you need to do to those members
                                UserPrincipal theUser = p as UserPrincipal;

                                if (theUser != null)
                                {
                                    DirectoryEntry directoryEntry = theUser.GetUnderlyingObject() as DirectoryEntry;
                                    if (directoryEntry.Properties.Contains("EmployeeID"))
                                    {
                                        receivers.Add(directoryEntry.Properties["EmployeeID"].Value.ToString());
                                    }
                                }
                            }
                        }

                    }
                }
                catch (Exception)
                {

                }
                
                List<Employees> employeeReceiversList = db.Employees.Where(a => receivers.Contains(a.EmployeeID.ToString()) ||
                                                                   receivers.Contains(a.JobTitles.JobTitleName.ToString())||
                                                                   receivers.Contains(a.EmployeeNumber.ToString())).ToList();
                Notifications n = null;
                DateTime now = DateTime.Now;
                foreach (var item in employeeReceiversList)
                {
                    n = new Notifications()
                    {
                        AppName = pren.appName,
                        CreationDate = now,
                        EmployeeCreatorID = pren.creatorID,
                        Icon = pren.icon,
                        IntendedEmployeeID = item.EmployeeID,
                        IntendedGroupID = 0,
                        Link = pren.link,
                        Message = pren.message,
                        Seen = false,
                        SeenDate = null,
                        Subject = pren.subject,
                        Type = pren.type
                    };
                    db.Notifications.Add(n);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return BadRequest("final" + ex.ToString() +"||||||||||||||"+ ex.InnerException.Message);
            }
            return Ok(1);

        }

        // DELETE: api/Notifications/5
        [ResponseType(typeof(Notifications))]
        public IHttpActionResult DeleteNotifications(int id)
        {
            Notifications notifications = db.Notifications.Find(id);
            if (notifications == null)
            {
                return NotFound();
            }

            db.Notifications.Remove(notifications);
            db.SaveChanges();

            return Ok(notifications);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NotificationsExists(int id)
        {
            return db.Notifications.Count(e => e.NotificationID == id) > 0;
        }
    }
}