﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using API.Models;


namespace API.Controllers
{
    public class MenuController : Controller
    {
        string concatenado = "";
        BAESystemsGuaymasDB db = new BAESystemsGuaymasDB();
        public string AsideMenu(string Len)
        {
            concatenado = "";
            var menus = ReadDads();
            foreach (var item in menus)
            {
                ConHijos(item, "Aside", Len,0);
            }
            return concatenado;
        }
        public string CreateAppMenu(string deparment, string user, string Len, int Dev)
        {
            concatenado = "";
            var menus = ReadAppMenu(deparment, user);
            foreach (var item in menus)
            {
                concatenado += "<li class='kt-menu__item  kt-menu__item--submenu kt-menu__item--rel "+item.AppName+"' data-ktmenu-submenu-toggle='click' aria-haspopup='true'><a href='javascript:;'class='kt-menu__link kt-menu__toggle'><span class='kt-menu__link-text'>" +
                  (Len == "es-ES" ? item.MenuNameSp : item.MenuNameEn) + "</span><i class='kt-menu__ver-arrow la la-angle-right'></i></a><div class='kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left'> <ul class='kt-menu__subnav'>";
                ConHijos(item, "AppMenu", Len, Dev);
                concatenado += "</ul></div></li>";
            }
            return concatenado;
        }
        public string CreateFooter()
        {
            concatenado = "";
            var menus = ReadFooter();
            foreach (var item in menus)
            {
                concatenado += "<a href='" + item.MenuHref + "' target='_blank' class='kt-footer__menu-link kt-link'>" + item.MenuNameSp + "</a>";
            }
            return concatenado;
        }
        #region MetodosCreamenu
        private void ConHijos(Menu1 mvm, string menutype, string Len,int? Dev)
        {
            if (mvm.Hijos != null)
            {
                if (menutype == "Aside")
                {
                    concatenado += "<li class='kt-menu__item  kt-menu__item--submenu' aria-haspopup='true' data-ktmenu-submenu-toggle='hover'><a href='javascript:;'class='kt-menu__link kt-menu__toggle'><i class='" + (string.IsNullOrEmpty(mvm.MenuIcon) ? " kt-menu__link-bullet kt-menu__link-bullet--dot" + "'><span></span>" : "kt-menu__link-icon " + mvm.MenuIcon + "'>") + "</i><span class='kt-menu__link-text'>" + (Len == "es-ES" ? mvm.MenuNameSp : mvm.MenuNameEn) + "</span><i class='kt-menu__ver-arrow la la-angle-right'></i></a><div class='kt-menu__submenu '><span class='kt-menu__arrow'></span><ul class='kt-menu__subnav'>";
                }
                else
                {
                    if (mvm.DadMenu != null)
                    {
                        concatenado += "<li class='kt-menu__item  kt-menu__item--submenu' aria-haspopup='true' data-ktmenu-submenu-toggle='hover'><a href='javascript:;' class='kt-menu__link kt-menu__toggle'><i class='kt-menu__link-icon " + mvm.MenuIcon + "'></i><span class='kt-menu__link-text'>" + (Len == "es-ES" ? mvm.MenuNameSp : mvm.MenuNameEn) + "</span>"+
                            "<i class='kt-menu__hor-arrow la la-angle-right'></i><i class='kt-menu__ver-arrow la la-angle-right'></i></a><div class='kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right'><span class='kt-menu__arrow'></span><ul class='kt-menu__subnav'>";
                    }
                }
                foreach (var hijito in mvm.Hijos)
                {
                    ConHijos(hijito, menutype, Len,Dev);
                }
                if (menutype == "Aside")
                {
                    concatenado += "</ul></div></li>";
                }
                else
                {
                    if (mvm.DadMenu != null)
                    {
                        concatenado += "</ul></div></li>";
                    }
                }
            }
            else
            {
                if (mvm.AppName == "Separator")
                {
                    concatenado += "<li class='kt-menu__section'><h4 class='kt-menu__section-text'>" + (Len == "es-ES" ? mvm.MenuNameSp : mvm.MenuNameEn) + "</h4><i class='kt-menu__section-icon flaticon-more-v2'></i></li>";
                }
                else
                {
                    if (menutype == "Aside"&& mvm.AppName=="Link")
                    {
                        concatenado += "<li class='kt-menu__item' aria-haspopup='true'><a target='_blank' href='" ;
                        concatenado += mvm.MenuHref;
                        concatenado +=  "' ";
                    }
                    else
                    {
                        concatenado += "<li class='kt-menu__item' aria-haspopup='true'><a href='";
                        if (Dev==0)
                        {
                            concatenado += mvm.MenuHref;
                        }
                        else
                        {
                            if (mvm.AppName=="EHPMS")
                            {
                                string replacement = "https://apps.bae.gym/BaseDatos/" + mvm.AppName;
                                int index = mvm.MenuHref.IndexOf(mvm.MenuDepartment) + mvm.AppName.Length;
                                //concatenado += mvm.MenuHref.Replace(replacement, "");
                                concatenado += mvm.MenuHref.Substring(index);
                            }
                            else
                            {
                                int index = mvm.MenuHref.IndexOf(mvm.AppName) + mvm.AppName.Length;
                                //concatenado += mvm.MenuHref.Replace(replacement, "");
                                concatenado += mvm.MenuHref;
                                //concatenado += mvm.MenuHref.Replace("https://apps.bae.gym/BaseDatos/" + mvm.MenuDepartment + "/" + mvm.AppName, "");
                            }
                            
                        }
                        
                        concatenado += "' ";
                    }
                    concatenado += "class='kt-menu__link " + mvm.MenuDepartment + "'><i class=' " + (string.IsNullOrEmpty(mvm.MenuIcon) ? " kt-menu__link-bullet kt-menu__link-bullet--dot" + "'><span></span>" : "kt-menu__link-icon " + mvm.MenuIcon + "'>") + "</i><span ";
                    concatenado += "class='kt-menu__link-text'>" + (Len == "es-ES" ? mvm.MenuNameSp : mvm.MenuNameEn) + "</span></a></li>";
                }
            }
        }
        private List<Menu1> ReadFooter()
        {
            var menus = db.Menu1Set.Where(a => a.DadMenu == null && a.Active && a.AppName == "Footer").ToList();
            foreach (var item in menus)
            {
                item.Hijos = GetHijos(item);
            }
            return menus;
        }
        private List<Menu1> ReadAppMenu(string department, string user)
        {
            List<string> groups = GetUserGroupMembership(user);
            if (groups.Count == 1)
            {
                groups = GetUserGroupMembership(user);
            }
            List<Menu1> menuus = db.MenuAccess.Where(a => groups.Contains(a.GroupName)).Select(a => a.Menu).Where(m => m.Active && m.MenuDepartment == department && m.DadMenu == null).Distinct().OrderBy(a=>a.SubMenuOrder).ToList();
            foreach (var item in menuus)
            {
                item.Hijos = GetHijos(groups, item);
            }
            return menuus;
        }
        public string ReadAppMenuString(string user)
        {
            List<string> groups = GetUserGroupMembership(user);
            if (groups.Count == 1)
            {
                groups = GetUserGroupMembership(user);
            }
            string r = "";
            foreach (var item in groups)
            {
                r += item.ToString() + ",";
            }
            return r;
        }
        public List<Menu1> GetHijos(List<string> groups, Menu1 papa)
        {
            List<Menu1> hijitos = db.MenuAccess.Where(a => groups.Contains(a.GroupName)).Select(a => a.Menu).Where(m => m.Active && m.MenuDepartment == papa.MenuDepartment && m.DadMenu == papa.MenuID).Distinct().OrderBy(a => a.SubMenuOrder).ToList();
            foreach (var item in hijitos)
            {
                item.Hijos = GetHijos(groups, item);
            }
            return hijitos.Count() > 0 ? hijitos : null;
        }
        public List<Menu1> GetHijos(Menu1 papa)
        {
            List<Menu1> hijitos = db.Menu1Set.Where(a => a.DadMenu == papa.MenuID && a.Active).ToList();
            foreach (var item in hijitos)
            {
                item.Hijos = GetHijos(item);
            }
            return hijitos.Count() > 0 ? hijitos : null;
        }
        private List<Menu1> ReadDads()
        {
            var menus = db.Menu1Set.Where(a => a.DadMenu == null && a.Active && a.MenuDepartment == "Menu").OrderBy(a => a.SubMenuOrder).ToList();
            foreach (var item in menus)
            {
                item.Hijos = GetHijos(item);
            }
            return menus;
        }
        private List<string> GetUserGroupMembership(string strUser)
        {
            List<string> groupss = new List<string>();
            groupss.Add("Default");
            try
            {
                List<GroupPrincipal> result = new List<GroupPrincipal>();
                PrincipalContext yourDomain = new PrincipalContext(ContextType.Domain);
                UserPrincipal user = UserPrincipal.FindByIdentity(yourDomain, strUser);
                if (user != null)
                {
                    PrincipalSearchResult<Principal> groups = user.GetAuthorizationGroups();
                    foreach (Principal p in groups)
                    {
                        groupss.Add((p.Name).ToString());
                    }
                }
            }
            catch (Exception)
            {
            }
            return groupss;
        }
        #endregion
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView();
        }
        public string GetActiveDirectoryGroups()
        {
            string groups = "";
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain);
            GroupPrincipal qbeGroup = new GroupPrincipal(ctx);
            PrincipalSearcher srch = new PrincipalSearcher(qbeGroup);
            foreach (var found in srch.FindAll())
            {
                groups += found.Name + ",";
            }
            groups += "Default";
            return groups;
        }
    }
}