﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using API.Models;

namespace API.Controllers
{
    public class MasterController : Controller
    {
        // GET: Master
        string concatenado = "";
        int nivel = 0;
        BAESystemsGuaymasDB db = new BAESystemsGuaymasDB();

        public string CreateMenu()
        {
            nivel = 0;
            return MenuCreator();
        }
        public string CreateMenuNotification()
        {
            nivel = 0;
            return MenuCreatorNotification();
        }
        public string CreateAppMenu(string app,string name,string permit)
        {
            nivel = 0;
            return AppMenuCreator(app,name, permit);
        }
        public string CreateFooter()
        {
            nivel = 0;
            return FooterCreator();
        }


        private string FooterCreator()
        {
            concatenado = "<footer><div class='footer-background'><div class='container'><div class='col-md-4 text-left'><a href='https://apps.bae.gym/' class='bae-logo-grey'>" +
                "</a><p style='font-size:11px;'><i class='fa fa-map-marker' aria-hidden='true'></i>&nbsp;Carretera Internacional KM 129 Salida Norte,<br/>" +
                "Parque Industrial Roca Fuerte, 85420<br/><i class='fa fa-phone' aria-hidden='true'></i>&nbsp;622 221 42 26</p></div>";

            var menus = ReadFooter();
            foreach (var item in menus)
            {
                ConHijos(item);
            }
            concatenado += "</div></div><div style='background-color:#000; padding:10px; font-size:small;'><div class='container'><p>&copy; " + System.DateTime.Now.Year.ToString() + "  - BAE Systems Guaymas</p></div></div></footer>";
            return concatenado;
        }
        private string AppMenuCreator(string app, string apptitle, string permit)
        {
            nivel = -1;
            concatenado = "<nav class='bae-header navbar-default' role='navigation'>" +
                "<div class='container' style='padding-left:15px;padding-right:15px;'>" +
                "<div class='navbar-header'>" +
                "<a href='/BaseDatos/" + app + "/'><h2 class='bae-title'>" + apptitle + "</h2></a></div><div class='collapse navbar-collapse bae-area-menu'" +
                " id='bs-example-navbar-collapse-2'><ul class='nav navbar-nav'>";
            var menus = ReadAppMenu(app, permit);
            foreach (var item in menus)
            {
                ConHijos(item);
            }
            concatenado += "</ul></div></div></nav>";
            return concatenado;
        }
        private string MenuCreator()
        {
            concatenado = "<nav class='navbar navbar-fixed-top' role='navigation' style='box-shadow: 0 0 2px rgba(0,0,0,.2);'><div class='container-fluid'>" +
                                        "<div class='navbar-header'><button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1'>" +
                                        "<span class='sr-only'>Toggle navigation</span><span class='icon-bar'></span><span class='icon-bar'></span><span class='icon-bar'></span>" +
                                        "</button><div><a href='https://apps.bae.gym/' class='baeImage'></a></div></div><div class='collapse navbar-collapse' " +
                                        "id='bs-example-navbar-collapse-1'><ul class='nav navbar-nav navbar-right'>";
            var menusdepartamento = ReadDepartments();
            foreach (var item in menusdepartamento)
            {
                ConDepartamentos(item);
            }
            var menus = ReadDads();
            foreach (var item in menus)
            {
                ConHijos(item);
            }
            concatenado += "</ul></div></div></nav>";


            return concatenado;
        }
        private string MenuCreatorNotification()
        {
            concatenado = "<nav class='navbar navbar-fixed-top' role='navigation' style='box-shadow: 0 0 2px rgba(0,0,0,.2);'><div class='container-fluid'>" +
                                        "<div class='navbar-header'><button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1'>" +
                                        "<span class='sr-only'>Toggle navigation</span><span class='icon-bar'></span><span class='icon-bar'></span><span class='icon-bar'></span>" +
                                        "</button><div><a href='https://apps.bae.gym/' class='baeImage'></a></div></div><div class='collapse navbar-collapse' " +
                                        "id='bs-example-navbar-collapse-1'><ul class='nav navbar-nav navbar-right'>";
            var menusdepartamento = ReadDepartments();
            foreach (var item in menusdepartamento)
            {
                ConDepartamentos(item);
            }
            var menus = ReadDads();
            foreach (var item in menus)
            {
                ConHijos(item);
            }
            concatenado += "<li class='nav-icon-btn nav-icon-btn-danger dropdown' id='NotificationDropdown'><a href='#notifications' class='dropdown-toggle' data-toggle='dropdown'><span id='NotificationNumber' class=''></span><i class='nav-icon fa fa-bullhorn'></i></a><div class='dropdown-menu widget-notifications no-padding' style='width: 300px'><div class='slimScrollDiv' style='position: relative; overflow: hidden; width: auto; height: 500px;'><div class='notifications-list' id='NotificationList' style='overflow: auto; width: auto; height: 500px;'></div><div class='slimScrollBar' style='background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height:195.925px;'></div><div class='slimScrollRail' style='width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;'></div></div></div></li>";
            concatenado += "</ul></div></div></nav>";


            return concatenado;
        }
        private void ConHijos(Menu mvm)
        {
            if (mvm.Hijos != null)
            {
                nivel += 1;
                if (mvm.AppName == "Footer")
                {
                    concatenado += "<div class='col-md-2 mx-auto'><h5 class='title mb-4 mt-3 font-bold'>" + mvm.MenuName + "</h5><ul class='list-unstyled'>";
                }
                else
                {
                    if (nivel < 1)
                    {
                        concatenado += "<li class='dropdown'><a href='" + mvm.MenuHref + "' class='dropdown-toggle' data-toggle='dropdown'>"
                                + mvm.MenuName + "<b class='caret'></b></a><ul class='dropdown-menu'>";

                    }
                    else
                    {
                        concatenado += "<li class='dropdown-submenu'><a href='" + mvm.MenuHref + "'>"
                               + mvm.MenuName + "</a><ul class='dropdown-menu'>";

                    }
                }


                foreach (var hijito in mvm.Hijos)
                {
                    ConHijos(hijito);

                }
                if (mvm.AppName == "Footer")
                {
                    concatenado += "</ul></div>";
                }
                else
                {
                    concatenado += "</ul></li>";
                }

                nivel--;

            }
            else
            {
                concatenado += "<li><a href='" + mvm.MenuHref + "'>";
                concatenado += string.IsNullOrEmpty(mvm.MenuIcon) ? mvm.MenuName + "</a></li>" : "<i class='" + mvm.MenuIcon + "' aria-hidden='true'></i>" + mvm.MenuName + "</a></li>";
            }



        }
        private void ConDepartamentos(Menu mvm)
        {
            int colactual = 1;
            int contador = 0;
            int max = mvm.Hijos.Count();//16
            int maxcol = max / 3;//1
            int sobrantes = max % 3;
            if (mvm.Hijos != null)
            {
                concatenado += " <li class='dropdown dropdown-large'><a href='" + mvm.MenuHref + "' class='dropdown-toggle' data-toggle='dropdown'>"
                            + mvm.MenuName + "<b class='caret'></b></a> <ul class='dropdown-menu dropdown-menu-large col-lg-4'>";
                foreach (var hijito in mvm.Hijos)
                {
                    if (contador == 0)
                    {
                        concatenado += "<li class='col-sm-4'><ul>";
                        if (colactual == 1)
                        {
                            concatenado += "<li><h4 class='bae-color'><i class='fa fa-fw fa-building' aria-hidden='true'></i>&nbsp;Departamentos</h4></li>";
                        }
                        else
                        {
                            concatenado += "<li><h4>&ensp;</h4></li>";
                        }
                        concatenado += "<li class='divider'></li>";
                    }
                    concatenado += "<li><a href='" + hijito.MenuHref + "'>" + hijito.MenuName + "</a></li>";
                    if (contador == maxcol - 1)
                    {
                        switch (colactual)
                        {
                            case 1:

                                colactual = 2;
                                contador = -1;
                                concatenado += "</ul></li>";
                                break;
                            case 2:
                                if (sobrantes == 2)
                                {
                                    contador--;
                                    sobrantes = 1;
                                }
                                else
                                {
                                    contador = -1;
                                    colactual = 3;
                                    concatenado += "</ul></li>";
                                }
                                break;
                            case 3:
                                if (sobrantes == 1)
                                {
                                    contador--;
                                    sobrantes = 0;
                                }
                                else
                                {
                                    concatenado += "</ul></li>";
                                }
                                break;
                        }
                    }
                    contador++;
                }
            }
            else
            {
                concatenado += "<li><a href='" + mvm.MenuHref + "'>" + mvm.MenuName + "</a></li>";
            }
            nivel--;
            concatenado += "</ul></li>";
        }
        private List<Menu> ReadFooter()
        {
            var menus = db.Menus.Where(a => a.DadMenu == null && a.Active && a.AppName == "Footer").ToList();

            return menus;
        }
        private List<Menu> ReadAppMenu(string app, string permit)
        {
            var b = new Menu();
            var menus = db.Menus.Where(a => a.DadMenu == null && a.Active && a.AppName == app && (a.MenuPermit == permit || a.MenuPermit == null || a.MenuPermit == "Default")).ToList();
            return menus;
        }
        private List<Menu> ReadDads()
        {
            var menus = db.Menus.Where(a => a.DadMenu == null && a.Active && a.MenuID >= 1000 && a.AppName == "Default").ToList();

            return menus;
        }
        private List<Menu> ReadDepartments()
        {
            var menus = db.Menus.Where(a => a.DadMenu == null && a.Active && a.MenuID < 1000 && a.AppName == "Default").ToList();
            return menus;
        }
    }
}