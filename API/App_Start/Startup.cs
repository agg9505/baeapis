﻿using System;
using Microsoft.AspNet.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin.Cors;
using Owin;

namespace API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //app.UseCors(builder =>
            //{
            //    builder.WithOrigins("https://example.com")
            //        .AllowAnyHeader()
            //        .WithMethods("GET", "POST")
            //        .AllowCredentials();
            //});
            app.Map("/signalr", map =>
            {
                
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration
                {
                    // You can enable JSONP by uncommenting line below.
                    // JSONP requests are insecure but some older browsers (and some
                    // versions of IE) require JSONP to work cross domain
                    //EnableJSONP = true
                };
                map.RunSignalR(hubConfiguration);
            });
            //app.MapSignalR();
        }
    }
}