﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API.Models
{
    [MetadataType(typeof(Menu))]
    public partial class Menu
    {
        public List<Menu> Hijos
        {
            get
            {
                BAESystemsGuaymasDB db = new BAESystemsGuaymasDB();
                List<Menu> hijitos = db.Menus.Where(a => a.DadMenu == MenuID && a.Active).ToList();
                return hijitos.Count() > 0 ? hijitos : null;
            }
            set
            {
            }
        }
    }
    [MetadataType(typeof(Menu))]
    public partial class Menu1
    {
        //public List<Menu1> Hijos
        //{
        //    get
        //    {
        //        BAESystemsGuaymasDB db = new BAESystemsGuaymasDB();
        //        List<Menu1> hijitos = db.Menu1Set.Where(a => a.DadMenu == MenuID && a.Active).ToList();
        //        return hijitos.Count() > 0 ? hijitos : null;
        //    }
        //    set
        //    {
        //    }
        //}
        public List<Menu1> Hijos { get; set; }

    }
        

    
    [MetadataType(typeof(LaptopsRecord))]
    public partial class LaptopsRecord
    {
        public string EmployeeNum{ get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm:ss}")]
        public System.DateTime Fecha { get; set; }
        public string Empleado { get; set; }
        public string Laptop { get; set; }
        public string Tipo { get; set; }
    }
}